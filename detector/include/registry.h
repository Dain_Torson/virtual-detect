#include <string>

namespace vmd
{
	auto readRegKey(const std::string& location, const std::string& name)->std::string;
	void writeRegKey(const std::string& location, const std::string& name, const std::string & value, int type);
	void deleteRegKey(const std::string& location, const std::string& name);
	void copyRegKey(const std::string& src, const std::string& dest);
}