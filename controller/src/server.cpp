#include "server.h"

#include "json.hpp"
#include "boost\asio.hpp"
#include <iostream>

using namespace boost::asio;

namespace vmd
{
	auto make_string(const streambuf & streambuf) -> std::string
	{
		return {buffers_begin(streambuf.data()), buffers_end(streambuf.data())};
	}

	inline auto deserialize(const std::string & message) -> std::vector<DetectInfo>
	{
		using nlohmann::json;

		json container = json::parse(message);
		return container;
	}

	auto Server::receiveData(unsigned short port) -> std::vector<DetectInfo>
	{
		ip::tcp::endpoint endpoint(ip::tcp::v4(), port);
		ip::tcp::acceptor acceptor(service, endpoint);
		ip::tcp::socket socket(service);

		acceptor.accept(socket);

		std::cout << "Connection established." << std::endl;

		boost::system::error_code error;

		streambuf buffer;
		auto bytesTransferred = read(socket, buffer, transfer_exactly(Server::INFO_MSG_LENGTH), error);
		auto infoLength = std::stoi(make_string(buffer));
		buffer.consume(bytesTransferred);

		std::cout << "Receiving " << infoLength << " bytes..." << std::endl;

		bytesTransferred = read(socket, buffer, transfer_exactly(infoLength), error);

		if (boost::system::errc::success != error)
		{
			std::cout << "Transfer failed with error code: " << error << std::endl;
			return {};
		}

		std::cout << "Transfer completed." << std::endl << std::endl;

		auto message = make_string(buffer);

		return deserialize(message);
	}
}