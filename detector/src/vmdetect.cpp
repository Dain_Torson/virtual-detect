#include "vmdetect.h"

#include "vmcpu.h"
#include "vmartifacts.h"
#include "vmpill.h"
#include "vmasm.h"
#include "vmnetwork.h"

#include <functional>
#include <algorithm>
#include <iostream>

namespace vmd
{
	const std::vector<std::function<DetectInfo()> > detectors =
	{
		checkCPUOverhead,
		checkMotherboardManufacturer,
		checkBIOSVersion,
		checkCPUCores,
		checkVBGuest,
		checkHDSize,
		checkIDT,
		checkLDT,
		checkGDT,
		checkAsmIN,
		checkSleepSkeeping,
		checkTLBAccess
	};

	const std::map<std::string, std::function<void()> > hiders = 
	{
		{"motherboard_manufacturer", hideMotherboardManufacturer},
		{"bios_version", hideBIOSVersion},
		{"vb_guest_additions", hideVBGuest},
		{"num_of_cpu_cores", hideCPUCores}
	};

	auto detect() -> std::vector<DetectInfo>
	{
		auto result = std::vector<DetectInfo>(detectors.size());

		std::transform(
			detectors.begin(),
			detectors.end(),
			result.begin(),
			[](std::function <DetectInfo()> detector) { return detector();}
		);

		return result;
	}

	void hide(const std::vector<DetectInfo> & data)
	{
		std::for_each(
			data.begin(),
			data.end(),
			[](DetectInfo info)
			{
				if (!info.detected)
				{
					return;
				}

				try
				{
					hiders.at(info.sign)();
				}
				catch (std::out_of_range &)
				{
					std::cout << "No hiding method for " << info.sign << std::endl;
				}
				catch (std::exception & ex)
				{
					std::cout << "Cannot hide " << info.sign << " : " << ex.what() << std::endl;
				}
			}
		);
	}
}
