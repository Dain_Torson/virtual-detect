#pragma once

#include "vmdata.h"

#include <vector>

namespace vmd
{
	auto detect()->std::vector<DetectInfo>;
	void hide(const std::vector<DetectInfo> & data);
}
