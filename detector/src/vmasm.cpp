#include "vmasm.h"

#include <iostream>
#include <stdio.h>

#include <windows.h> 
#include <excpt.h>

namespace vmd
{
	auto executeAsmIN() -> bool
	{
		__try
		{
			__asm
			{
				mov eax, 564d5868h; 'VMXh'
				mov ecx, 0ah; get VMware version
				mov dx, 5658h; 'VX'
				in  eax, dx
			}
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
			return false;
		}

		return true;
	}

	auto checkAsmIN() -> DetectInfo
	{
		auto info = DetectInfo();
		info.sign = "assembly_in";
		info.detected = executeAsmIN();
		return info;
	}
}
