#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <Windows.h>

#include "vmdetect.h"
#include "client.h"

using namespace std;

auto logging = false;
auto hide = false;

void writeLog(vector<vmd::DetectInfo> & data)
{
	std::ofstream logFile("log.txt");

	for (auto & info : data)
	{
		logFile << info;
	}

	logFile.close();
}

void parseArg(char * arg)
{
	if (0 == strcmp(arg, "-l"))
	{
		logging = true;
	}
	else if (0 == strcmp(arg, "-h"))
	{
		hide = false;
	}
}

int main(unsigned int argc, char * argv[])
{
	if (argc > 1 && 0 == strcmp(argv[1], "-help"))
	{
		cout << "Usage detector.exe [<flags>]" << endl;
		cout << "Flags: \n\t -l for loging into text file;" << endl;
		cout << "\t -h for hiding artifacts." << endl;
		system("PAUSE");
		return -1;
	}

	for (auto argInx = 1u; argInx < argc; ++argInx)
	{
		parseArg(argv[argInx]);
	}

	auto data = vmd::detect();

	for (auto & info : data)
	{
		cout << info;
	}

	if (logging)
	{
		writeLog(data);
	}

	if (hide)
	{
		vmd::hide(data);
	}

	system("PAUSE");
    return 0;
}