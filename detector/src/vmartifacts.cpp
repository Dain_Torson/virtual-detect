#pragma comment(lib, "Wbemuuid")

#include "vmartifacts.h"
#include "registry.h"

#include <windows.h>
#include <iostream>
#include <vector>
#include <algorithm>

#include "Wbemidl.h"


#define MIN_DRIVE_SIZE_MB 32000

using namespace std::string_literals;

std::vector<std::string> keyWordList = {"virtualbox", "vmvare", "vbox"};
std::vector<std::string> singleCoreCPUs = { "celeron", "sempron", "atom" };

namespace vmd
{
	//template<class T> void typeCheck(T type);
	
	void getDriveGeometry(LPSTR szPath, DISK_GEOMETRY *pdg)
	{
		HANDLE hDevice = INVALID_HANDLE_VALUE;  
		DWORD junk = 0;                     // discard results

		hDevice = CreateFileA(szPath,  0, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr,  OPEN_EXISTING,  0, nullptr);           

		if (hDevice == INVALID_HANDLE_VALUE)    // cannot open the drive
		{
			throw std::invalid_argument("Cannot open the drive.");
		}

		//typeCheck(DeviceIoControl);
		//std::cout << "DeviceIoControl pointer  " << (void *)(*(DWORD *)((char *)DeviceIoControl + 1) + (char *)DeviceIoControl + 5) << std::endl;

		if(DeviceIoControl(hDevice, IOCTL_DISK_GET_DRIVE_GEOMETRY, nullptr, 0,  pdg, sizeof(*pdg),  &junk, (LPOVERLAPPED)nullptr) == false)
		{
			CloseHandle(hDevice);
			throw std::invalid_argument("Cannot retrieve information.");
		}
		CloseHandle(hDevice);
	}

	auto checkRegKeyValue(std::string &location, std::string &name,
		std::vector <std::string> checkList, bool reverseCheck = false) -> DetectInfo
	{
		auto info = DetectInfo();
		std::string value;

		try
		{
			value = readRegKey(location, name);
		}
#ifdef _DEBUG
		catch (std::exception &ex)
#else
		catch (std::exception &)
#endif
		{
#ifdef _DEBUG
			std::cout << ex.what() << std::endl;
#endif
			info.detected = true;
			info.message = "Failed to open registry key";
			return info;
		}

		std::transform(value.begin(), value.end(), value.begin(), tolower);

		std::vector<bool> findList(checkList.size());
		std::transform
		(
			checkList.begin(),
			checkList.end(),
			findList.begin(),
			[&value](std::string &keyWord)
			{
				return value.find(keyWord) != std::string::npos;
			}
		);

		bool toFind = !reverseCheck;

		info.message = name + " is "s + value;
		info.detected = std::find(findList.begin(), findList.end(), toFind) != findList.end();

		return info;
	}

	auto checkMotherboardManufacturer() -> DetectInfo
	{
		std::string location = "HARDWARE\\DESCRIPTION\\System\\BIOS";
		std::string name = "BaseBoardManufacturer";

		auto info = checkRegKeyValue(location, name, keyWordList);
		info.sign = "motherboard_manufacturer";
		return info;
	}

	void hideMotherboardManufacturer()
	{
		std::string location = "HARDWARE\\DESCRIPTION\\System\\BIOS";
		std::string name = "BaseBoardManufacturer";
		std::string value = "msi";

		writeRegKey(location, name, value, REG_SZ);
	}

	auto checkBIOSVersion() -> DetectInfo
	{
		std::string location = "HARDWARE\\DESCRIPTION\\System";
		std::string name = "SystemBiosVersion";

		auto info = checkRegKeyValue(location, name, keyWordList);
		info.sign = "bios_version";
		return info;
	}

	void hideBIOSVersion()
	{
		std::string location = "HARDWARE\\DESCRIPTION\\System";
		std::string name = "SystemBiosVersion";
		std::string value = "acrsys";

		writeRegKey(location, name, value, REG_MULTI_SZ);
	}

	auto checkCPUCores() -> DetectInfo
	{
		std::string firstCoreLocation = "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0";
		std::string secondCoreLocation = "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\1";
		std::string name = "ProcessorNameString";

		auto info = checkRegKeyValue(firstCoreLocation, name, singleCoreCPUs, true);
		info.sign = "num_of_cpu_cores";

		try
		{
			auto value = readRegKey(secondCoreLocation, name);
		}
		catch(std::exception &)
		{
			info.message += " with one core";
			return info;
		}

		info.detected = false;
		return info;
	}

	void hideCPUCores()
	{
		std::string firstCoreLocation = "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0";
		std::string secondCoreLocation = "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\1";

		copyRegKey(firstCoreLocation, secondCoreLocation);
	}

	auto checkVBGuest() -> DetectInfo
	{
		auto info = DetectInfo();
		info.sign = "vb_guest_additions";
		info.detected = true;

		std::string location = "SOFTWARE\\Oracle\\VirtualBox Guest Additions";
		std::string name = "Version";

		try
		{
			auto value = readRegKey(location, name);
			info.message = "VBox Guest Additions version: "s + value;
		}
		catch (std::exception &)
		{
			info.detected = false;
		}
		return info;
	}

	void hideVBGuest()
	{
		std::string location = "SOFTWARE\\Oracle";
		std::string name = "VirtualBox Guest Additions";
		deleteRegKey(location, name);
	}

	auto checkHDSize()->DetectInfo
	{
		auto info = DetectInfo();
		info.sign = "hd_size";

		DISK_GEOMETRY data;

		try
		{

			getDriveGeometry("\\\\.\\PhysicalDrive0", &data);

			auto size = (data.Cylinders.QuadPart * data.TracksPerCylinder * data.SectorsPerTrack * data.BytesPerSector) / 1000000.;
			info.message = "HD size: "s + std::to_string(size) + " Mb";
			info.detected = size < MIN_DRIVE_SIZE_MB;
		}
#ifdef _DEBUG
		catch (std::exception &ex)
#else
		catch (std::exception &)
#endif
		{
#ifdef _DEBUG
			std::cout << ex.what() << std::endl;
			info.message = "Failed to query disk geometry.";
			info.detected = false;
#endif
		}
		return info;
	}
}