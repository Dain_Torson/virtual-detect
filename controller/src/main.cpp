#include "server.h"
#include "scsgenerator.h"

#include <iostream>
#include <fstream>
#include <conio.h>


using namespace std;

int main(int argc, char * argv[])
{
	auto logging = false;

	if (2 == argc && 0 == strcmp(argv[1], "-l"))
	{
		cout << "Args:" << endl;
		cout << argv[1] << endl;
		logging = true;
	}

	vmd::Server server;
	auto data = server.receiveData(8080);

	for (auto & info : data)
	{
		cout << info << endl;
	}

	vmd::generateSCS(data);
	
	cout << "Press any key..." << endl;
	getch();
    return 0;
}