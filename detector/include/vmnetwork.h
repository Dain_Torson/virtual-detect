#pragma once

#include "vmdata.h"

namespace vmd
{
	auto checkSleepSkeeping() -> DetectInfo;
}