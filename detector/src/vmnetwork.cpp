#include "vmnetwork.h"

#include <windows.h>
#include <string>
#include <chrono>

#include "client.h"

#define GOOGLE_URL "http://google.com"

namespace vmd
{

	inline auto getElapsedSecondsOfDay(const std::string & header) -> std::chrono::seconds
	{
		auto endOfDate = header.find("GMT");
		return std::chrono::seconds(

			    std::stoi(header.substr(endOfDate - 9, 2)) * 3600 +
				std::stoi(header.substr(endOfDate - 6, 2)) * 60 +
				std::stoi(header.substr(endOfDate - 3, 2))
				);
	}


	auto checkSleepSkeeping() -> DetectInfo
	{

		using namespace std::chrono;

		auto info = DetectInfo();
		info.sign = "sleep_skeep";

		try
		{
			Client & client = Client::Instance();

			auto result1 = client.readUrl(GOOGLE_URL);
			auto httpTimePoint1 = getElapsedSecondsOfDay(result1.first);
			auto realTimePoint1 = system_clock::now();

			auto sleepTime = 60 * 1000;
#ifdef _DEBUG	
			sleepTime = 12*1000;
#endif
			Sleep(sleepTime);

			auto result2 = client.readUrl(GOOGLE_URL);
			auto httpTimePoint2 = getElapsedSecondsOfDay(result2.first);
			auto realTimePoint2 = system_clock::now();

			auto httpDiff = httpTimePoint2.count() - httpTimePoint1.count();
			auto realDiff = duration_cast<seconds>(realTimePoint2 - realTimePoint1).count();

			if (realDiff - httpDiff > 10)
			{
				info.detected = true;
				info.message = "Sleep skeeping detected, the difference is ";
				info.message += std::to_string((realDiff - httpDiff));
			}
		}
		catch(std::exception & ex)
		{
			info.detected = true;
			info.message = ex.what();
		}

		return info;
	}
}
