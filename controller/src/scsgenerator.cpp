#include "scsgenerator.h"

#include <fstream>
#include <iterator>
#include <algorithm>
#include <sstream>
#include <ctime>

#include <mstch/mstch.hpp>

namespace vmd
{
	void generateSCS(std::vector<DetectInfo> & data)
	{
		std::ifstream templateFile("template/concept_execution_enviroment.scs", std::ios::binary);
		std::istreambuf_iterator<char> begin(templateFile), end;
		std::string content(begin, end);

		mstch::array artifacts;

		std::for_each(data.begin(), data.end(), [&artifacts](DetectInfo & info)
			{if (info.detected) artifacts.push_back(mstch::map{ { "artifact", info.sign} });}
		);

		time_t t = time(0);
		struct tm * now = localtime(&t);

		std::stringstream ss;
		ss << now->tm_mday << now->tm_mon << (now->tm_year - 100) <<
			now->tm_hour << now->tm_min << now->tm_sec;
		auto id = ss.str();

		mstch::map context{{"artifacts", artifacts}};
		context["id"] = id;
		auto output = mstch::render(content, context);

		std::ofstream outputFile("enviroment" + id + ".scs", std::ios::binary);
		outputFile << output;
	}
}