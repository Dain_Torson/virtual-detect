#include "vmpill.h"

#include <sstream>

namespace vmd
{
	auto checkIDT()->DetectInfo
	{
		auto info = DetectInfo();
		info.sign = "idt";

		unsigned char m[6];

		__asm sidt m;
		
		std::stringstream ss;
		ss << "IDTR: ";

		for (auto idx = 0u; idx < 6; ++idx)
		{
			ss << "Ox" << std::hex << static_cast<int>(m[idx]) << " ";
		}

		info.message = ss.str();
		info.detected = (m[0] != 0x00 && m[1] != 0x00) ? 1 : 0;

		info.message = ss.str();
		info.detected = (m[5] > 0xd0) ? 1 : 0;

		return info;
	}

	auto checkLDT()->DetectInfo
	{
		auto info = DetectInfo();
		info.sign = "ldt";

		unsigned char m[6];

		__asm sldt m;

		std::stringstream ss;
		ss << "LDTR: ";
		for(auto idx = 0u; idx < 6; ++ idx)
		{
			ss << "Ox" << std::hex << static_cast<int>(m[idx]) << " ";
		}

		info.message = ss.str();
		info.detected = (m[0] != 0x00 && m[1] != 0x00) ? 1 : 0;

		return info;
	}

	auto checkGDT()->DetectInfo
	{
		auto info = DetectInfo();
		info.sign = "gdt";

		unsigned char m[6];

		__asm sgdt m;

		std::stringstream ss;
		ss << "GDTR: ";

		for (auto idx = 0u; idx < 6; ++idx)
		{
			ss << "Ox" << std::hex << static_cast<int>(m[idx]) << " ";
		}

		info.message = ss.str();
		info.detected = (m[5] > 0xd0) ? 1 : 0;

		return info;
	}
}
