#include "vmdata.h"

#include <vector>
#include <string>
#include <sstream>
#include <tuple>
#include <utility>
#include <windows.h>

namespace vmd
{
	class Client
	{
	public:
		static Client& Instance()
		{
			static Client instance;
			return instance;
		}

		auto readUrl(char *szUrl)->std::pair<std::string, std::string>;
		void sendData(std::vector<DetectInfo> & data, const char * server, WORD port);

		static const short INFO_MSG_LENGTH = 4;

	private:
		Client();
		~Client();

		Client(const Client &) = delete;
		Client(Client &&) = delete;
		Client & operator= (const Client &) = delete;
	};
}




