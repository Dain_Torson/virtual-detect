#pragma once

#include <iostream>
#include<string>
#include<vector>

#include "json.hpp"

namespace vmd
{

	struct DetectInfo
	{
		std::string sign; //detection sign
		bool detected;
		std::string message; //explanatory message
		std::string details; //additional info if needed
	};

	std::ostream& operator <<(std::ostream& stream, const DetectInfo & info);

	void to_json(nlohmann::json & j, const DetectInfo & info);

	void from_json(const nlohmann::json & j, DetectInfo & info);
}