#pragma once

#include "vmdata.h"

namespace vmd
{
	auto checkCPUOverhead() -> DetectInfo;
	auto checkTLBAccess() -> DetectInfo;
}
