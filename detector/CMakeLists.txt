project(detector)
cmake_minimum_required(VERSION 2.8)

SET (CMAKE_CXX_STANDARD 14)

include_directories("./include/")

SET(SOURCES 

	src/main.cpp
	src/vmartifacts.cpp
	src/vmasm.cpp
	src/vmcpu.cpp
	src/vmdetect.cpp
	src/vmnetwork.cpp
	src/vmpill.cpp
	src/vmdata.cpp
	src/client.cpp
	src/registry.cpp
)

SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -D_DEBUG")


add_executable(detector ${SOURCES})

