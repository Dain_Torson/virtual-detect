#include "pkHookMgr.h"

#include <cstdint>
#include <sstream>

#include "capstone/include/capstone.h"

static const BYTE jmpCode = 0xE9;
static const DWORD CodeFragmSize = 5;

void pkHookMgr::PatchFunction(const char * dllName, const char * funcName, PBYTE fFunc)
{
	HMODULE hBase = LoadLibraryA(dllName);
    if (hBase == nullptr)
    {
        std::stringstream ss;
        ss << "Unable to load " << dllName;
        throw std::system_error(std::error_code((int)GetLastError(), std::generic_category()), ss.str().c_str());
    }
        
	PBYTE pFunc = (PBYTE)GetProcAddress(hBase, funcName);
    if (pFunc == nullptr)
    {
        std::stringstream ss;
        ss << "Unable to find " << funcName << " in " << dllName;
        throw std::system_error(std::error_code((int)GetLastError(), std::generic_category()), ss.str().c_str());
    }

    DWORD fragmSize = GetCodeLength(pFunc);
    DWORD oldProt, tmp;
    std::unique_ptr<BYTE[], FuncDesc::HeapDeleter<BYTE[]>> stub((BYTE*)HeapAlloc(hExecHeap, 0, fragmSize + CodeFragmSize), FuncDesc::HeapDeleter<BYTE[]>(hExecHeap));
    PBYTE nextPtr = stub.get();
    memcpy(nextPtr, pFunc, fragmSize);
    if (*nextPtr == jmpCode)
        *(DWORD*)(nextPtr + 1) += (pFunc - nextPtr);

    nextPtr += fragmSize;
    *(nextPtr++) = jmpCode;
    PBYTE pFuncEntry = pFunc + fragmSize;
    int delta = pFuncEntry - (nextPtr + sizeof(DWORD));
    memcpy(nextPtr, &delta, sizeof(delta));
    nextPtr += sizeof(DWORD);

    FuncDesc d{ std::move(stub), pFunc, fragmSize };

    VirtualProtect(pFunc, CodeFragmSize, PAGE_EXECUTE_READWRITE, &oldProt);
    *pFunc = jmpCode;
    *(int*)(pFunc + 1) = (fFunc - (pFunc + CodeFragmSize));
    VirtualProtect(pFunc, CodeFragmSize, oldProt, &tmp);

    auto it = stubs.lower_bound(fFunc);
    if (it != stubs.end() && it->first == fFunc)
        throw std::logic_error("Function already hooked");
    stubs.emplace_hint(it, fFunc, std::move(d));

    std::string s = dllName;
    s += "!";
    s += funcName;
    dict[s] = fFunc;
}

void pkHookMgr::UnpatchFunction(PBYTE pFunc, PBYTE stub, DWORD len)
{
	DWORD oldProt, tmp;
	VirtualProtect(pFunc, len, PAGE_EXECUTE_READWRITE, &oldProt);
	memcpy(pFunc, stub, len);
	VirtualProtect(pFunc, len, oldProt, &tmp);
}

pkHookMgr::~pkHookMgr()
{
	for (auto& s : stubs)
		UnpatchFunction(s.second.head, s.second.stub.get(), s.second.Len);
}

void pkHookMgr::UnpatchFunction(const char * dllName, const char * funcName)
{
	std::string s = dllName;
	s += "!";
	s += funcName;
	auto it = dict.find(s);
	if (it != dict.end())
	{
		auto el = stubs.find(it->second);
        if (el == stubs.end())
        {
            std::stringstream ss;
            ss << "Unable to unpatch function " << dllName << '!' << funcName;
            throw std::system_error(std::error_code(-1, std::generic_category()), ss.str().c_str());
        }
		UnpatchFunction(el->second.head, el->second.stub.get(), el->second.Len);
	}
}

DWORD pkHookMgr::GetCodeLength(PBYTE addr)
{
	DWORD res = 0;
	csh handle;
	cs_err err = cs_open(CS_ARCH_X86, CS_MODE_32, &handle);
	if (err == CS_ERR_OK)
	{
		cs_insn *insn = nullptr;
		size_t decodedInstructionsCount = cs_disasm(handle, addr, 0x20, 0, 5, &insn);
		if (decodedInstructionsCount != 0)
		{
			int len = 0;
			for (size_t i = 0; i < decodedInstructionsCount; ++i)
			{
				const cs_insn & di = insn[i];
				len += di.size;
				if (len >= CodeFragmSize)
				{
					res = len;
					break;
				}
			}
			cs_free(insn, decodedInstructionsCount);
		}
	}
    else
        throw std::system_error(std::error_code((int)err, std::generic_category()), "Disassembler internal error");

	return res;
}
