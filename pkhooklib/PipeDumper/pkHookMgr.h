#pragma once
#ifndef   __PKHOOKMGR__
#define   __PKHOOKMGR__

#include <windows.h>

#include <map>
#include <memory>
#include <string>

class pkHookMgr
{
	struct FuncDesc
	{
        template <class T>
        class HeapDeleter
        {
        private:
            HANDLE hHeap;
        public:
            HeapDeleter(HANDLE heap) : hHeap(heap) {}
            void operator() (T p) { HeapFree(hHeap, 0, p); }
        };
		std::unique_ptr<BYTE[], HeapDeleter<BYTE[]>> stub;
		PBYTE head;
		DWORD Len;
	};
	std::map<PVOID, FuncDesc>    stubs;
	std::map<std::string, PVOID> dict;

    HANDLE hExecHeap = HeapCreate(HEAP_CREATE_ENABLE_EXECUTE, 0x10000, 0);

	void UnpatchFunction(PBYTE pFunc, PBYTE stub, DWORD len);

	DWORD GetCodeLength(PBYTE addr);

public:
	void PatchFunction(const char * dllName, const char * funcName, PBYTE fFunc);
	void UnpatchFunction(const char * dllName, const char * funcName);
	
	template <class hf, class ...Args>
    auto CallOriginalFunction(hf func, Args... args)
	{
        auto it = stubs.find(func);
        if (it == stubs.end())
            throw std::logic_error("Internal error");
		return ((hf)(it->second.stub.get()))(args...);
	}

	~pkHookMgr();

	std::map<std::string, PVOID> GetPatches()
	{
		return dict;
	}
};

#endif // __PKHOOKMGR__
