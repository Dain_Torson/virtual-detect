#include "registry.h"

#include <windows.h>
#include <vector>

namespace vmd
{
	auto readRegKey(const std::string& location, const std::string& name) -> std::string
	{
		HKEY key;
		std::vector<TCHAR> value;
		DWORD valueLength;

		auto error = RegOpenKeyExA(HKEY_LOCAL_MACHINE, location.c_str(), 0, KEY_QUERY_VALUE, &key);
		if (error != ERROR_SUCCESS)
		{
			RegCloseKey(key);
			throw std::invalid_argument(std::string("Cannot open registry key. Error code: ") + std::to_string(error));
		}

		error = RegQueryValueExA(key, name.c_str(), nullptr, nullptr, nullptr, &valueLength);
		if (error != ERROR_SUCCESS)
		{
			RegCloseKey(key);
			throw std::invalid_argument(std::string("Cannot query value length. Error code: ") + std::to_string(error));
		}

		value.resize(valueLength);
		error = RegQueryValueExA(key, name.c_str(), nullptr, nullptr, (LPBYTE)&value[0], &valueLength);
		if (error != ERROR_SUCCESS)
		{
			RegCloseKey(key);
			throw std::invalid_argument(std::string("Cannot registry value. Error code: ") + std::to_string(error));
		}

		std::string stringValue(value.begin(), value.end());

		RegCloseKey(key);
		return stringValue;
	}

	void writeRegKey(const std::string& location, const std::string& name, const std::string & value, int type)
	{
		HKEY key;

		auto error = RegOpenKeyExA(HKEY_LOCAL_MACHINE, location.c_str(), 0, KEY_WRITE, &key);
		if (error != ERROR_SUCCESS)
		{
			RegCloseKey(key);
			throw std::invalid_argument(std::string("Cannot open registry key. Error code: ") + std::to_string(error));
		}

		error = RegSetValueExA(key, name.c_str(), 0, type, (LPBYTE)value.c_str(), value.size());
		if (error != ERROR_SUCCESS)
		{
			RegCloseKey(key);
			throw std::invalid_argument(std::string("Cannot open registry key. Error code: ") + std::to_string(error));
		}

		RegCloseKey(key);
	}

	void deleteRegKey(const std::string& location, const std::string& name)
	{
		HKEY key;

		auto error = RegOpenKeyExA(HKEY_LOCAL_MACHINE, location.c_str(), 0, KEY_ALL_ACCESS, &key);
		if (error != ERROR_SUCCESS)
		{
			RegCloseKey(key);
			throw std::invalid_argument(std::string("Cannot open registry key. Error code: ") + std::to_string(error));
		}

		error = RegDeleteTree(key, name.c_str());
		if (error != ERROR_SUCCESS)
		{
			RegCloseKey(key);
			throw std::invalid_argument(std::string("Cannot delete registry key. Error code: ") + std::to_string(error));
		}

		RegCloseKey(key);
	}

	void copyRegKey(const std::string& src, const std::string& dest)
	{
		HKEY srcKey;

		auto error = RegOpenKeyExA(HKEY_LOCAL_MACHINE, src.c_str(), 0, KEY_ALL_ACCESS, &srcKey);
		if (error != ERROR_SUCCESS)
		{
			RegCloseKey(srcKey);
			throw std::invalid_argument(std::string("Cannot open registry key. Error code: ") + std::to_string(error));
		}

		HKEY destKey;

		error = RegCreateKeyExA(HKEY_LOCAL_MACHINE, dest.c_str(), 0, nullptr, 0, KEY_ALL_ACCESS, nullptr, &destKey, nullptr);
		if (error != ERROR_SUCCESS)
		{
			RegCloseKey(destKey);
			throw std::invalid_argument(std::string("Cannot create registry key. Error code: ") + std::to_string(error));
		}

		error = RegCopyTree(srcKey, nullptr, destKey);
		if (error != ERROR_SUCCESS)
		{
			RegCloseKey(srcKey);
			RegCloseKey(destKey);
			throw std::invalid_argument(std::string("Cannot copy registry key. Error code: ") + std::to_string(error));
		}

		RegCloseKey(srcKey);
		RegCloseKey(destKey);
	}
}