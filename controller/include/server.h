#include "boost/asio/io_service.hpp"

#include "vmdata.h"

namespace vmd
{
	class Server
	{
	public:
		auto receiveData(unsigned short port)-> std::vector<DetectInfo>;
		static const short INFO_MSG_LENGTH = 4;

	private:
		boost::asio::io_service service;
	};
}