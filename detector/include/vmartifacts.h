#pragma once

#include "vmdata.h"

namespace vmd
{
	auto checkMotherboardManufacturer() -> DetectInfo;
	auto checkBIOSVersion() -> DetectInfo;
	auto checkCPUCores()->DetectInfo;
	auto checkVBGuest()->DetectInfo;
	auto checkHDSize()->DetectInfo;

	void hideMotherboardManufacturer();
	void hideBIOSVersion();
	void hideVBGuest();
	void hideCPUCores();
}