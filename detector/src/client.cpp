#include "client.h"

#include <exception>
#include <algorithm>

#pragma comment (lib, "Ws2_32.lib")

namespace vmd
{
	Client::Client()
	{
		WSADATA wsaData;

		if (WSAStartup(0x101, &wsaData) != 0)
		{
			throw std::ios_base::failure("Failed to init winsock library");
		}
	}

	Client::~Client()
	{
		WSACleanup();
	}

	auto parseUrl(char *mUrl) -> std::tuple<std::string, std::string, std::string>
	{

		std::string serverName;
		std::string filePath;
		std::string fileName;
		std::string url(mUrl);

		if (url.substr(0, 7) == "http://")
		{
			url.erase(0, 7);
		}

		if (url.substr(0, 8) == "https://")
		{
			url.erase(0, 8);
		}

		auto endOfDomain = url.find('/');
		if (endOfDomain != std::string::npos)
		{
			serverName = url.substr(0, endOfDomain);
			filePath = url.substr(endOfDomain);
			endOfDomain = filePath.rfind('/');
			fileName = filePath.substr(endOfDomain + 1);
		}

		else
		{
			serverName = url;
			filePath = "/";
			fileName = "";
		}

		return std::make_tuple(serverName, filePath, fileName);
	}


	auto connectToServer(std::string & szServerName, WORD portNum) -> SOCKET
	{
		struct sockaddr_in server;

		auto conn = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (conn == INVALID_SOCKET)
		{
			return NULL;
		}

		if (INADDR_NONE == inet_addr(szServerName.c_str()))
		{
			auto hp = gethostbyname(szServerName.c_str());
			if (nullptr == hp)
			{
				closesocket(conn);
				return NULL;
			}

			server.sin_addr.s_addr = *(reinterpret_cast<unsigned long*>(hp->h_addr));
		}
		else
		{
			server.sin_addr.s_addr = inet_addr(szServerName.c_str());
		}

		server.sin_family = AF_INET;
		server.sin_port = htons(portNum);

		auto status = connect(conn, reinterpret_cast<struct sockaddr*>(&server), sizeof(server));

		if (status)
		{
			closesocket(conn);
			return NULL;
		}

		return conn;
	}

	auto checkInternetConnection() -> bool
	{
		std::string googleDNS = "8.8.8.8";
		return NULL != connectToServer(googleDNS, 80);
	}

	auto getHeaderLength(std::string & content) -> int
	{
		std::string srchStr1 = "\r\n\r\n", srchStr2 = "\n\r\n\r";

		auto findPos = content.find(srchStr1);
		if (std::string::npos != findPos)
		{
			return findPos + srchStr1.size();
		}

		findPos = content.find(srchStr1);
		if (std::string::npos != findPos)
		{
			return findPos + srchStr2.size();
		}

		return -1;
	}

	auto Client::readUrl(char *szUrl) -> std::pair<std::string, std::string>
	{
		std::string server, filepath, filename;

		std::tie(server, filepath, filename) = parseUrl(szUrl);

		// TODO: ��������, ��� �� ��������
		/*if(!checkInternetConnection())
		{
			throw std::ios_base::failure("No internet connection");
		}*/

		auto conn = connectToServer(server, 80);

		std::stringstream ss;

		ss << "GET " << filepath << " HTTP/1.0\r" << std::endl <<
			"Host: " << server << "\r\n\r" << std::endl;

		auto toSent = ss.str();
		send(conn, toSent.c_str(), toSent.size(), 0);

		const auto bufSize = 512;
		char readBuffer[bufSize];

		ss.str("");
		ss.clear();

		auto totalReadSize = 0ul;

		while (true)
		{
			memset(readBuffer, 0, bufSize);
			auto thisReadSize = recv(conn, readBuffer, bufSize, 0);

			if (thisReadSize <= 0)
			{
				break;
			}

			totalReadSize += thisReadSize;
			ss << readBuffer;
		}

		closesocket(conn);

		if(0 == totalReadSize)
		{
			throw std::ios_base::failure("Could not establish connection");
		}

		auto message = ss.str();
		auto headerLen = getHeaderLength(message);
		if (-1 != headerLen)
		{
			return std::make_pair(message.substr(0, headerLen), message.substr(headerLen));
		}

		return std::make_pair(std::string(), message);
	}

	inline auto adjustInfoLength(const std::string & info) -> std::string
	{
		if (info.length() > Client::INFO_MSG_LENGTH)
		{
			return std::string(Client::INFO_MSG_LENGTH, '0');
		}
		
		auto diff = Client::INFO_MSG_LENGTH - info.length();
		return diff > 0 ? std::string(diff, '0') + info : info;
	}

	void Client::sendData(std::vector<DetectInfo> & data, const char * server, WORD port)
	{
		using nlohmann::json;

		json messageJSON(data);
		auto dump = messageJSON.dump();
		auto size = std::to_string(dump.size());

		std::string message = adjustInfoLength(size) + dump;
		std::string serv = server;

		auto conn = connectToServer(serv, port);
		send(conn, message.c_str(), message.size(), 0);

		closesocket(conn);
	}
}
