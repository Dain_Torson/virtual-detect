// injectloader.cpp : ���������� dll ��� ��������
//Gerasimenko Alexey, 2010

#define DEBUG

#define SIZE_OF_PUSH_OFFSET 5
#define SIZE_OF_CALL 5
#define SIZE_OF_RETN 3

#define OPCODE_CALL  0xE8
#define OPCODE_PUSH_OFFSET 0x68
#define OPCODE_RETN 0xC2 

#define MEMSIZE SIZE_OF_PUSH_OFFSET+SIZE_OF_CALL+SIZE_OF_RETN


#include "stdafx.h"
#include <stdlib.h>
#include <windows.h>
#include <psapi.h>
#include <string.h>

void DebugPrint(char *msg)
{
	printf("%s GetLastError reports %d\n", msg, GetLastError());
}

void WriteCALL(HANDLE h, void *dest, void *tocall)
{
	unsigned char call[SIZE_OF_CALL];
	call[0] = OPCODE_CALL;
	DWORD wr;
	DWORD addr = (DWORD)tocall - (DWORD)dest - SIZE_OF_CALL;
	memcpy(&call[1], &addr, sizeof(addr));
	if(!WriteProcessMemory(h, dest, &call, SIZE_OF_CALL, &wr) | (wr != SIZE_OF_CALL))
	{
#ifdef DEBUG
		  DebugPrint("Cannot write process memory.\n");
#endif
		return ;
	}
}

void WritePUSHoffset(HANDLE h, void *dest, void *topush)
{
	unsigned char push[SIZE_OF_PUSH_OFFSET];
	push[0] = OPCODE_PUSH_OFFSET;
	DWORD wr;
	DWORD addr = (DWORD)topush;
	memcpy(&push[1], &addr, sizeof(addr));
	if(!WriteProcessMemory(h, dest, &push, SIZE_OF_PUSH_OFFSET, &wr) | (wr != SIZE_OF_PUSH_OFFSET))
	{
#ifdef DEBUG
		  DebugPrint("Cannot write process memory.\n");
#endif
		return ;
	}
}

void WriteRETN(HANDLE h, void *dest, unsigned short toret)
{
	unsigned char ret[SIZE_OF_RETN];
	DWORD wr;
	ret[0] = OPCODE_RETN;
	memcpy(&ret[1], &toret, sizeof(toret));
	if(!WriteProcessMemory(h, dest, &ret, SIZE_OF_RETN, &wr) | (wr != SIZE_OF_RETN))
	{
#ifdef DEBUG
		  DebugPrint("Cannot write process memory.\n");
#endif
		return ;
	}
}

HANDLE checkProcessName(DWORD processID, char * required)
{
	char szProcessName[MAX_PATH];

	// Get a handle to the process.

	HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
		PROCESS_VM_READ,
		FALSE, processID);

	// Get the process name.

	if (NULL != hProcess)
	{
		HMODULE hMod;
		DWORD cbNeeded;

		if (EnumProcessModules(hProcess, &hMod, sizeof(hMod),
			&cbNeeded))
		{
			GetModuleBaseNameA(hProcess, hMod, szProcessName,
				sizeof(szProcessName) / sizeof(TCHAR));
		}
	}

	if(0 == strcmp(szProcessName, required))
	{
		return hProcess;
	}

	// Release the handle to the process.

	CloseHandle(hProcess);
	return  0;
}


int main(int argc, char * argv[])
{
	if(argc != 2)
	{
		DebugPrint("Using: injectloader[.exe] name\n");
		return 0;
	}

	/*DWORD aProcesses[1024], cbNeeded;

	if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
	{
		return 1;
	}

	DWORD cProcesses = cbNeeded / sizeof(DWORD);

	HANDLE h;

	for (auto i = 0u; i < cProcesses; i++)
	{
		if (aProcesses[i] != 0)
		{
			h = checkProcessName(aProcesses[i], argv[1]);\
			if(0 != h)
			{
				break;
			}
		}
	}*/


	STARTUPINFOA info = {sizeof(info)};
	PROCESS_INFORMATION processInfo;
	BOOL err = CreateProcessA("c:\\detector.exe", "detector.exe 127.0.0.1", nullptr, nullptr, TRUE, CREATE_SUSPENDED, nullptr, nullptr, &info, &processInfo);

	HANDLE h = processInfo.hProcess;

	if (h==NULL)
	{
#ifdef DEBUG
		char msg[100];
		sprintf(msg, "Cannot open process with name %s.\n", argv[1]);
		DebugPrint(msg);
#endif
		return 0;
	}
	
	//�������� ������ � �������� ������������ ������� ��������
	void *newmem = VirtualAllocEx(h, NULL, MEMSIZE, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if(newmem == NULL)
	{
#ifdef DEBUG
		  DebugPrint("Cannot allocate memory.\n");
#endif
		return 0;
	}
	
	DWORD wr;
	char m1[17] = "c:\\inject.dll";
	void *hm1 = VirtualAllocEx(h, NULL, lstrlenA(m1)+1, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if(!WriteProcessMemory(h, hm1, m1, lstrlenA(m1)+1, &wr) | (wr != lstrlenA(m1) + 1))
	{
#ifdef DEBUG
		  DebugPrint("Cannot write process memory.\n");
#endif
		return 0;
	}

	//����� ��������� ����� API �������
	void *func = GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");
	if(func==NULL)
	{
#ifdef DEBUG
		DebugPrint("Cannot load function LoadLibraryA from module kernel32.dll.\n");
#endif
		return 0;
	}

	WritePUSHoffset(h, newmem, hm1);
	newmem = (void*)((DWORD)newmem + SIZE_OF_PUSH_OFFSET);

	WriteCALL(h, newmem, func);
	newmem = (void*)((DWORD)newmem + SIZE_OF_CALL);

	WriteRETN(h, newmem, sizeof(DWORD));

	newmem = (void*)((DWORD)newmem - SIZE_OF_CALL - SIZE_OF_PUSH_OFFSET);

	auto eventHandle = CreateEventA(nullptr, TRUE, FALSE, "InjectEvent");

	if(CreateRemoteThread(h, NULL, 0, (LPTHREAD_START_ROUTINE)newmem, NULL, 0, NULL) == NULL)
	{
#ifdef DEBUG
		  DebugPrint("Cannot create remote thread.\n");
#endif
		return 0;
	}

	WaitForSingleObject(eventHandle, INFINITE);
	Sleep(2000);
	ResumeThread(processInfo.hThread);
	return 0;
}

