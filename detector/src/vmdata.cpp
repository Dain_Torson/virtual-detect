#include "vmdata.h"

namespace vmd
{
	std::ostream& operator <<(std::ostream& stream, const DetectInfo & info)
	{

		stream << "Sign: " << info.sign << std::endl;
		stream << "Detected: " << info.detected << std::endl;
		stream << "Message: " << info.message << std::endl;
		stream << std::endl;

		return stream;
	}

	void to_json(nlohmann::json & j, const DetectInfo & info)
	{

		j = nlohmann::json{ { "sign", info.sign },{ "detected", info.detected },{ "message", info.message } };
	}

	void from_json(const nlohmann::json & j, DetectInfo & info)
	{
		info.sign = j["sign"].get<std::string>();
		info.detected = j["detected"].get<bool>();
		info.message = j["message"].get<std::string>();
	}

}