#include "vmcpu.h"

#include <stdint.h>
#include <intrin.h>
#include <sstream>
#include <cstdlib>
#include <random>
#include <memory>
#include <iostream>

using namespace std::string_literals;

namespace vmd
{

	auto checkCPUOverhead() -> DetectInfo
	{
		auto info = DetectInfo();
		info.sign = "cpu_overhead";

		auto first = __rdtsc();
		auto second = __rdtsc();
		auto diff = second - first;

		info.detected = diff < 20 || diff > 400;
		info.message = "The difference beetween two rdtsc calls is "s + std::to_string(diff);

		return info;
	}

	auto speedTest(int * arr, size_t until) -> long long
	{
		
		auto iterations = 1000;
		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_int_distribution<int> dist(0, until - 1);

		auto first = __rdtsc();
		for(auto iter = 0; iter < iterations; ++ iter)
		{
			auto idx = dist(mt);
			auto temp = arr[idx];
		}
		auto second = __rdtsc();

		return second - first;
	}


	auto checkTLBAccess() -> DetectInfo
	{
		auto info = DetectInfo();
		info.sign = "tlb_access";

		auto iterations = 500000u;

#ifdef _DEBUG	
		iterations = 1000;
#endif

		const size_t size = 1000;

		std::unique_ptr<int[]> arr(new int[size]);

		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_int_distribution<int> dist(0, 100);

		auto initSpeed = speedTest(arr.get(), size);

		for(auto iter = 0u; iter < iterations; ++ iter)
		{
			for(auto idx = 0u; idx < size; ++ idx)
			{
				arr[idx] = dist(mt);
			}
		}

		_asm cpuid;

		auto finalSpeed = speedTest(arr.get(), size);
		info.detected = finalSpeed > initSpeed;
		info.message = "Overhead is "s + std::to_string(finalSpeed - initSpeed) + " cycles"s;

		return info;
	}
}