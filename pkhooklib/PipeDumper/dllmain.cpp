#include <stdio.h>
#include <string.h>
#include <vector>

#include <iostream>
#include <random>
#include <ctime>

#include "pkHookMgr.h"

HANDLE hExploitPipe = (HANDLE)(-1);
std::string pipeName = /*"\\\\.\\pipe\\status_8443"*/ "\\\\.\\pipe\\status_";
std::string dumpPath = "D:";
FILE * exploit_log = nullptr;

pkHookMgr mgr;

static HANDLE WINAPI fCreateNamedPipeA( _In_     LPCSTR                lpName,
										_In_     DWORD                 dwOpenMode,
										_In_     DWORD                 dwPipeMode,
										_In_     DWORD                 nMaxInstances,
										_In_     DWORD                 nOutBufferSize,
										_In_     DWORD                 nInBufferSize,
										_In_     DWORD                 nDefaultTimeOut,
										_In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes)
{
	HANDLE hPipe = mgr.CallOriginalFunction(fCreateNamedPipeA, lpName, dwOpenMode, dwPipeMode, nMaxInstances, nOutBufferSize, nInBufferSize, nDefaultTimeOut, lpSecurityAttributes);
	std::string sarg = lpName;
	if ((sarg.length() >= pipeName.length()) && stricmp(sarg.substr(0, pipeName.length()).c_str(), pipeName.c_str()) == 0)
	{
		exploit_log = fopen(dumpPath.c_str(), "wb");
		hExploitPipe = hPipe;
	}
		
	return hPipe;
}

DWORD WINAPI fNtReadFile(_In_     HANDLE           FileHandle,
						 _In_opt_ HANDLE           Event,
						 _In_opt_ PVOID            ApcRoutine,
						 _In_opt_ PVOID            ApcContext,
						 _Out_    PVOID            IoStatusBlock,
						 _Out_    PVOID            Buffer,
						 _In_     ULONG            Length,
						 _In_opt_ PLARGE_INTEGER   ByteOffset,
						 _In_opt_ PULONG           Key)
{
	DWORD res = mgr.CallOriginalFunction(fNtReadFile, FileHandle, Event, ApcRoutine, ApcContext, IoStatusBlock, Buffer, Length, ByteOffset, Key);
	if (FileHandle == hExploitPipe)
	{
		fwrite(Buffer, 1, Length, exploit_log);
		fflush(exploit_log);
		memset(Buffer, 0xC3, Length);
	}
	return res;
}

auto fakeTime(char * buf, size_t size) -> bool
{
	auto gmtIndex = -1;
	for(auto idx = 0u; idx < size - 2; ++ idx)
	{
		if(buf[idx] == 'G' && buf[idx + 1] == 'M' && buf[idx + 2] == 'T')
		{
			gmtIndex = idx;
			break;
		}
	}

	if(-1 == gmtIndex)
	{
		return false;
	}

	time_t t = time(0);
	struct tm * now = localtime(&t);

	char sec[4], min[4];

	char * formatSec = now->tm_sec < 10 ? "0%d" : "%d";
	char * formatMin = now->tm_min < 10 ? "0%d" : "%d";

	sprintf(sec, formatSec, now->tm_sec);
	sprintf(min, formatMin, now->tm_min);

	buf[gmtIndex - 3] = sec[0];
	buf[gmtIndex - 2] = sec[1];

	buf[gmtIndex - 6] = min[0];
	buf[gmtIndex - 5] = min[1];

	return true;
}

bool checkIfDriveHandle(HANDLE hDevice)
{
	HANDLE hDrive = CreateFileA("\\\\.\\PhysicalDrive0", 0, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, 0, nullptr);
	if (hDrive == hDevice)
	{
		std::cout << "DRIVE REQUEST DETECTED" << std::endl;
		return true;
	}

	return false;
}

BOOL WINAPI fDeviceIoControl(
	_In_        HANDLE       hDevice,
	_In_        DWORD        dwIoControlCode,
	_In_opt_    LPVOID       lpInBuffer,
	_In_        DWORD        nInBufferSize,
	_Out_opt_   LPVOID       lpOutBuffer,
	_In_        DWORD        nOutBufferSize,
	_Out_opt_   LPDWORD      lpBytesReturned,
	_Inout_opt_ LPOVERLAPPED lpOverlapped
)
{
#ifdef _DEBUG
	std::cerr << "Entering DeviceIoControl" << std::endl;
#endif // _DEBUG
	auto res = mgr.CallOriginalFunction(fDeviceIoControl, hDevice, dwIoControlCode, lpInBuffer,
		nInBufferSize, lpOutBuffer, nOutBufferSize, lpBytesReturned, lpOverlapped);

	if (!res || (IOCTL_DISK_GET_DRIVE_GEOMETRY != dwIoControlCode))
	{
#ifdef _DEBUG
		std::cerr << "Exiting DeviceIoControl" << std::endl;
#endif // _DEBUG
		return res;
	}

	DISK_GEOMETRY * data = (DISK_GEOMETRY *)lpOutBuffer;
	auto size = (data->Cylinders.QuadPart * data->TracksPerCylinder * data->SectorsPerTrack * data->BytesPerSector) / 1000000.;
	auto fraction = size / 100000;

	if (fraction < 1)
	{
		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_real_distribution<double> dist(1.0, 10.0);

		auto factor = dist(mt);
		data->Cylinders.QuadPart = data->Cylinders.QuadPart * factor / fraction;
	}
#ifdef _DEBUG
	std::cerr << "Exiting DeviceIoControl" << std::endl;
#endif // _DEBUG
	return STATUS_INVALID_PARAMETER;
}

int WINAPI winSockRecv(
	_In_  SOCKET s,
	_Out_ char   *buf,
	_In_  int    len,
	_In_  int    flags
)
{
#ifdef _DEBUG
	std::cerr << "Entering winsock" << std::endl;
#endif // _DEBUG
	auto res = mgr.CallOriginalFunction(winSockRecv, s, buf, len, flags);

	if (0 != res && res != SOCKET_ERROR)
	{
		fakeTime(buf, res);
#ifdef _DEBUG
		std::cerr << buf << std::endl;
#endif // _DEBUG

		
	}
#ifdef _DEBUG
	std::cerr << "Exiting winsock" << std::endl;
#endif // _DEBUG
	return res;
}

static void Init()
{
	std::vector<char> buffer(0x1000);
	DWORD res = GetEnvironmentVariableA("EXPLOIT_PIPE", &buffer[0], buffer.size());
	if (res != 0)
		pipeName = &buffer[0];
	
	res = GetEnvironmentVariableA("EXPLOIT_DUMP_DIR", &buffer[0], buffer.size());
	if (res != 0)
		dumpPath = &buffer[0];

	sprintf(&buffer[0], "%s\\exploit_dump_%u.bin", dumpPath.c_str(), (unsigned)GetCurrentProcessId());
	dumpPath = &buffer[0];

	mgr.PatchFunction("WS2_32", "recv", (PBYTE)winSockRecv);
	mgr.PatchFunction("KERNEL32", "DeviceIoControl", (PBYTE)fDeviceIoControl);

	auto eventHandle = OpenEvent(EVENT_ALL_ACCESS, FALSE, "InjectEvent");
	SetEvent(eventHandle);
}

static void Uninit()
{
	if (exploit_log != nullptr)
		fclose(exploit_log);
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		Init();
		break;
	case DLL_PROCESS_DETACH:
		Uninit();
		break;
	default:
		break;
	}
	return TRUE;
}

