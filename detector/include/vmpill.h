#pragma once
#include "vmdata.h"

namespace vmd
{
	auto checkIDT() -> DetectInfo;
	auto checkLDT() -> DetectInfo;
	auto checkGDT() -> DetectInfo;
}
